import { useTranslations } from 'next-intl';
import Card from './Card';
import Image from 'next/image';

const Starlight = () => {
  const StarLight = useTranslations('StarLight');
  const game_play_list = StarLight.raw('game_play_list');
  const free_game_list = StarLight.raw('free_game_list');
  const special_symbols = StarLight.raw('special_symbols');
  const good_luck_description = StarLight.raw('good_luck_description');
  const game_dividend_description = StarLight.raw('game_dividend_description');
  const disclaimer_description = StarLight.raw('disclaimer_description');
  const odds_table_list = [
    {
      image: '/images/starlight/SYM_M1.png',
    },
    {
      image: '/images/starlight/SYM_M2.png',
    },
    {
      image: '/images/starlight/SYM_M3.png',
    },
    {
      image: '/images/starlight/SYM_M4.png',
    },
    {
      image: '/images/starlight/SYM_M5.png',
    },
    {
      image: '/images/starlight/SYM_M6.png',
    },
    {
      image: '/images/starlight/SYM_M7.png',
    },
    {
      image: '/images/starlight/SYM_M8.png',
    },
  ];

  console.log(StarLight.raw('free_game_list')[0].split('{image}'));

  return (
    <div className='flex flex-col space-y-3 bg-[#000000D9] px-10 text-white md:px-[50px]'>
      <div className='flex justify-center'>
        <span className='text-4xl font-bold'>{StarLight('title')}</span>
      </div>
      <hr className=' border-[#595959] ' />
      <div className=' flex justify-center'>
        <span className='text-3xl font-bold'>{StarLight('game_play')}</span>
      </div>
      <ul className='flex list-disc flex-col space-y-6  pl-[40px] text-2xl font-normal'>
        {game_play_list.map((game: string, index: number) => (
          <li key={index}>{game}</li>
        ))}
      </ul>
      <hr className=' border-[#595959]' />
      <div className=' flex justify-center'>
        <span className='text-3xl font-bold'>{StarLight('free_game')}</span>
      </div>
      <ul className='flex list-disc flex-col space-y-6 pl-[40px] text-2xl font-normal'>
        {free_game_list.map((game: string, index: number) => (
          <li key={index}>
            <div className='h-16'>
              {game.split('{image}').map((item, index) => {
                if (index % 2 === 0) {
                  return item;
                } else {
                  return (
                    <>
                      <div className='mx-1  inline-block  w-[60px] align-middle'>
                        <Image
                          key={index}
                          src='/images/starlight/SYM_C1.png'
                          alt='Starlight'
                          width={164}
                          height={176}
                          priority
                        ></Image>
                      </div>
                      {item}
                    </>
                  );
                }
              })}
            </div>
          </li>
        ))}
      </ul>
      <hr className=' border-[#595959] ' />
      <Card>
        <div className='flex flex-row'>
          <div className='flex basis-2/5 items-center  justify-center md:basis-1/5'>
            <Image
              src='/images/starlight/SYM_C1.png'
              alt='Starlight'
              width={164}
              height={176}
              style={{ objectFit: 'cover' }}
            ></Image>
          </div>
          <div className='flex basis-3/5 items-center md:basis-4/5'>
            <ul className='flex list-disc flex-col space-y-6 pl-[40px] text-2xl font-normal'>
              {special_symbols
                .filter((_: any, index: number) => index < 3)
                .map((game: string, index: number) => (
                  <li key={index}>{game}</li>
                ))}
            </ul>
          </div>
        </div>
      </Card>
      <Card>
        <div className='flex flex-row'>
          <div className='flex basis-2/5  flex-row items-center justify-center md:basis-1/5'>
            <div className='flex flex-col'>
              <Image
                src='/images/starlight/SYM_C2_01.png'
                alt='Starlight'
                width={120}
                height={87}
                style={{ objectFit: 'cover' }}
              ></Image>
              <Image
                src='/images/starlight/SYM_C2_02.png'
                alt='Starlight'
                width={120}
                height={87}
                style={{ objectFit: 'cover' }}
              ></Image>
            </div>
            <div className='flex flex-col'>
              <Image
                src='/images/starlight/SYM_C2_03.png'
                alt='Starlight'
                width={120}
                height={87}
                style={{ objectFit: 'cover' }}
              ></Image>
              <Image
                src='/images/starlight/SYM_C2_04.png'
                alt='Starlight'
                width={120}
                height={87}
                style={{ objectFit: 'cover' }}
              ></Image>
            </div>
          </div>
          <div className='flex  basis-3/5  items-center md:basis-4/5'>
            <ul className='flex list-disc flex-col space-y-6  pl-[40px] text-2xl font-normal'>
              {special_symbols
                .filter((_: any, index: number) => index >= 3)
                .map((game: string, index: number) => (
                  <li key={index}>{game}</li>
                ))}
            </ul>
          </div>
        </div>
      </Card>
      <hr className='my-5 border-[#595959] ' />
      <div className='flex justify-center'>
        <span className='text-4xl font-bold'>{StarLight('odds_table')}</span>
      </div>
      <Card className='border-red-500'>
        <div className='flex flex-row items-center justify-center space-x-10 '>
          <div className='flex h-60 items-center '>
            <Image
              className='scale-[1.5]'
              src='/images/starlight/SYM_C1.png'
              alt='Starlight'
              width={164}
              height={176}
              style={{ objectFit: 'cover' }}
            ></Image>
          </div>

          <div className='flex flex-col text-2xl font-bold'>
            <div>
              <span className='text-[#FFB03A]'>6：</span> <span>100 x BET</span>
            </div>
            <div>
              <span className='text-[#FFB03A]'>5：</span> <span>5 x BET</span>
            </div>
            <div>
              <span className='text-[#FFB03A]'>4：</span> <span>3 x BET</span>
            </div>
          </div>
        </div>
      </Card>

      <div className=' grid grid-cols-2 gap-2'>
        {odds_table_list.map((item, index) => (
          <Card key={index}>
            <div className='flex flex-row items-center justify-center '>
              <div className='flex h-52 basis-1/2 items-center justify-center'>
                <Image
                  src={item.image}
                  alt='Starlight'
                  width={214}
                  height={162}
                  style={{ objectFit: 'cover' }}
                ></Image>
              </div>
              <div className='flex basis-1/2 flex-col  justify-center text-lg font-bold md:text-2xl'>
                {[
                  { symbol: '12-30', odds: '50 x BET' },
                  { symbol: '10-11', odds: '25 x BET' },
                  { symbol: '8-9', odds: '10 x BET' },
                ].map((data, index) => (
                  <div key={index}>
                    <span className='text-[#FFB03A]'>{data.symbol}：</span>
                    <span>{data.odds}</span>
                  </div>
                ))}
              </div>
            </div>
          </Card>
        ))}
      </div>
      <Card>
        <div className='flex flex-row items-center justify-center    '>
          <div className='flex h-52 items-center '>
            <Image
              src='/images/starlight/SYM_M9.png'
              alt='Starlight'
              width={214}
              height={163}
              style={{ objectFit: 'cover' }}
            ></Image>
          </div>
          <div className='flex flex-col  text-lg font-bold md:text-2xl'>
            {[
              { symbol: '12-30', odds: '50 x BET' },
              { symbol: '10-11', odds: '25 x BET' },
              { symbol: '8-9', odds: '10 x BET' },
            ].map((data, index) => (
              <div key={index}>
                <span className='text-[#FFB03A]'>{data.symbol}：</span>
                <span>{data.odds}</span>
              </div>
            ))}
          </div>
        </div>
      </Card>
      <hr className=' border-[#595959] ' />
      <div className=' flex justify-center'>
        <span className='text-3xl font-bold'>{StarLight('good_luck')}</span>
      </div>
      <ul className='flex list-disc flex-col space-y-6  pl-[40px] text-2xl font-normal'>
        {good_luck_description.map((game: string, index: number) => (
          <li key={index}>{game}</li>
        ))}
      </ul>
      <hr className=' border-[#595959] ' />
      <div className=' flex justify-center'>
        <span className='text-3xl font-bold'>{StarLight('game_dividend')}</span>
      </div>
      <ul className='flex list-disc flex-col space-y-6  pl-[40px] text-2xl font-normal'>
        {game_dividend_description.map((game: string, index: number) => (
          <li key={index}>{game}</li>
        ))}
      </ul>
      <hr className=' border-[#595959]' />
      <div className=' flex justify-center'>
        <span className='text-3xl font-bold'>{StarLight('disclaimer')}</span>
      </div>
      <ul className='flex list-disc flex-col space-y-6  pl-[40px] text-2xl font-normal'>
        {disclaimer_description.map((game: string, index: number) => (
          <li key={index}>{game}</li>
        ))}
      </ul>
    </div>
  );
};

export default Starlight;
