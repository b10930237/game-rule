interface CardProps {
  children?: React.ReactNode;
  className?: string;
}

export default function Card({ children, className }: CardProps) {
  return (
    <div className={`rounded-xl border-4 p-3 ${className}`}>{children}</div>
  );
}
