import { useTranslations } from 'next-intl';

export default function Index() {
  const t = useTranslations('Index');

  return (
    <div>
      <h1>{t('welcome')}</h1>
      <h1>{t('description')}</h1>
    </div>
  );
}
