const PayTableCard = ({
  img,
  params: { isCredit, bet, denom },
  pays,
  textCenter = false,
}: {
  img: string;
  params: {
    isCredit: boolean;
    bet: number;
    denom: number;
  };
  pays?: {
    pay: number;
    symbol: string;
  }[];
  textCenter?: boolean;
}) => {
  return (
    <div className='flex flex-col space-y-2 '>
      <div>
        <img src={`/images/e/${img}`} alt={img} />
      </div>
      <div className={`flex flex-col ${textCenter && 'items-center'}`}>
        {pays?.map(({ pay, symbol }, index: number) => {
          const result = isCredit ? bet * pay : (bet * pay * denom) / 10000;
          return (
            <div key={index} className='flex space-x-2 text-2xl'>
              <span className='text-[#FFB03A]'>{symbol}:</span>
              <span>
                {isCredit || `$`}
                {result}
              </span>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default PayTableCard;
