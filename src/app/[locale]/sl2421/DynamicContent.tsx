interface DynamicContentProps {
  text: string;
}

function DynamicContent({ text }: DynamicContentProps) {
  const replaceTextWithImages = (inputText: string) => {
    const regex = /\{([^}]+\.(jpg|jpeg|png|gif))\}/g;

    return inputText.replace(regex, (match: string, imageName: string) => {
      return `<img class="inline-block w-8 sm:w-12" src="/images/e/${imageName}" alt="${imageName}" />`;
    });
  };

  const createMarkup = () => {
    const htmlString = replaceTextWithImages(text);
    return { __html: htmlString };
  };

  return <div dangerouslySetInnerHTML={createMarkup()} />;
}

export default DynamicContent;
