'use client';
import { useMessages } from 'next-intl';
import { useSearchParams } from 'next/navigation';
import DynamicContent from './DynamicContent';
import PayTableCard from './PayTableCard';

const Sl2421 = () => {
  const searchParams = useSearchParams();

  const bet: number = searchParams.get('bet')
    ? parseInt(searchParams.get('bet') || '')
    : 0;
  const denom: number = searchParams.get('denom')
    ? parseInt(searchParams.get('denom') || '')
    : 0;
  const isCredit: boolean =
    searchParams.get('isCredit') === 'true' ? true : false;

  const params = {
    bet,
    denom,
    isCredit,
  };

  const messages = useMessages();
  const Sl2421 = messages.Sl2421 as any;

  return (
    <div className='flex flex-col space-y-8 bg-[#000000] px-12 py-20 text-white'>
      <div className='flex justify-center '>
        <span className='text-xl font-bold text-[#FFE132] md:text-4xl'>
          {Sl2421.pay_table}
        </span>
      </div>
      <ul className='flex  list-disc list-image-[url(/images/e/icon_star.png)] flex-col  space-y-6 pl-5 text-xl font-normal md:text-2xl'>
        {Sl2421.pay_table_description.map((game: string, index: number) => (
          <li key={index}>{game}</li>
        ))}
      </ul>

      <div className='grid grid-cols-3 gap-y-10'>
        <div className='col-span-3 flex items-center justify-center'>
          <PayTableCard
            textCenter
            img='SYM_WW 1.png'
            params={params}
            pays={[
              {
                pay: 50,
                symbol: '5',
              },
            ]}
          />
        </div>
        <div className='col-span-1 flex items-center justify-center'>
          <PayTableCard
            img='SYM_M1 1.png'
            params={params}
            pays={[
              {
                pay: 50,
                symbol: '5',
              },
              {
                pay: 31.25,
                symbol: '4',
              },
              {
                pay: 5,
                symbol: '3',
              },
            ]}
          />
        </div>
        <div className='col-span-1 col-end-4 flex items-center justify-center'>
          <PayTableCard
            img='SYM_M2 1.png'
            params={params}
            pays={[
              {
                pay: 31.25,
                symbol: '5',
              },
              {
                pay: 15,
                symbol: '4',
              },
              {
                pay: 3.75,
                symbol: '3',
              },
            ]}
          />
        </div>
        <div className='col-span-1 flex items-center justify-center'>
          <PayTableCard
            img='SYM_M3 1.png'
            params={params}
            pays={[
              {
                pay: 25,
                symbol: '5',
              },
              {
                pay: 10,
                symbol: '4',
              },
              {
                pay: 5,
                symbol: '3',
              },
            ]}
          />
        </div>
        <div className='col-span-1 col-end-4 flex items-center justify-center'>
          <PayTableCard
            img='SYM_M4 1.png'
            params={params}
            pays={[
              {
                pay: 25,
                symbol: '5',
              },
              {
                pay: 10,
                symbol: '4',
              },
              {
                pay: 3.75,
                symbol: '3',
              },
            ]}
          />
        </div>
        <div className='col-span-1 flex items-center justify-center'>
          <PayTableCard
            img='SYM_M5 1.png'
            params={params}
            pays={[
              {
                pay: 15,
                symbol: '5',
              },
              {
                pay: 6.25,
                symbol: '4',
              },
              {
                pay: 2.5,
                symbol: '3',
              },
            ]}
          />
        </div>
        <div className='col-span-1 col-end-4 flex items-center justify-center'>
          <PayTableCard
            img='SYM_M6 1.png'
            params={params}
            pays={[
              {
                pay: 15,
                symbol: '5',
              },
              {
                pay: 6.25,
                symbol: '4',
              },
              {
                pay: 2.5,
                symbol: '3',
              },
            ]}
          />
        </div>
        <div className='col-span-1 flex items-center justify-center'>
          <PayTableCard
            img='SYM_M7 1.png'
            params={params}
            pays={[
              {
                pay: 25,
                symbol: '5',
              },
              {
                pay: 10,
                symbol: '4',
              },
              {
                pay: 5,
                symbol: '3',
              },
            ]}
          />
        </div>
        <div className='col-span-1 col-end-4 flex items-center justify-center'>
          <PayTableCard
            img='SYM_M8 1.png'
            params={params}
            pays={[
              {
                pay: 15,
                symbol: '5',
              },
              {
                pay: 5,
                symbol: '4',
              },
              {
                pay: 1.25,
                symbol: '3',
              },
            ]}
          />
        </div>
        <div className='col-span-1 flex items-center justify-center'>
          <PayTableCard
            img='SYM_M9 1.png'
            params={params}
            pays={[
              {
                pay: 15,
                symbol: '5',
              },
              {
                pay: 5,
                symbol: '4',
              },
              {
                pay: 1.25,
                symbol: '3',
              },
            ]}
          />
        </div>
        <div className='col-span-1 col-end-4 flex items-center justify-center'>
          <PayTableCard
            img='SYM_M10 1.png'
            params={params}
            pays={[
              {
                pay: 15,
                symbol: '5',
              },
              {
                pay: 5,
                symbol: '4',
              },
              {
                pay: 1.25,
                symbol: '3',
              },
            ]}
          />
        </div>
      </div>

      <hr className='my-4 h-1 w-full border-0 bg-fade-hr' />

      <div className='flex justify-center'>
        <span className='text-xl font-bold text-[#FFE132] md:text-4xl'>
          {Sl2421.wild_symbol}
        </span>
      </div>
      <div className='flex flex-row space-x-5'>
        <div className='flex basis-1/4 items-center justify-center'>
          <img src='/images/e/SYM_WW 11.png' width={130} height={130} alt='' />
        </div>
        <div className='flex basis-3/4 items-center justify-center'>
          <ul className='flex list-disc list-image-[url(/images/e/icon_star.png)] flex-col space-y-6 pl-5 text-xl font-normal md:text-2xl'>
            {Sl2421.wild_symbol_description.map(
              (game: string, index: number) => (
                <li key={index}>
                  <DynamicContent text={game} />
                </li>
              )
            )}
          </ul>
        </div>
      </div>

      <hr className='my-4 h-1 w-full border-0 bg-fade-hr' />

      <div className='flex justify-center'>
        <span className='text-xl font-bold text-[#FFE132] md:text-4xl'>
          {Sl2421.mystery_reveal}
        </span>
      </div>

      <div className='flex flex-row space-x-4'>
        <div className='flex basis-1/4 items-center justify-center'>
          <img src='/images/e/SYM_MY 1.png' width={130} height={121} alt='' />
        </div>
        <div className='flex basis-3/4 items-center justify-center'>
          <ul className='flex list-disc list-image-[url(/images/e/icon_star.png)] flex-col space-y-6 pl-5 text-xl font-normal md:text-2xl'>
            {Sl2421.mystery_reveal_description.map(
              (game: string, index: number) => (
                <li key={index}>
                  <DynamicContent text={game} />
                </li>
              )
            )}
          </ul>
        </div>
      </div>

      <div className='flex justify-center'>
        <span className='text-xl font-bold text-[#FFE132] md:text-4xl'>
          {Sl2421.free_games_feature}
        </span>
      </div>
      <ul className='flex list-disc list-image-[url(/images/e/icon_star.png)] flex-col space-y-6 pl-5 text-xl font-normal md:text-2xl'>
        {Sl2421.free_games_feature_description.map(
          (game: string, index: number) => (
            <li key={index}>
              <DynamicContent text={game} />
            </li>
          )
        )}
      </ul>

      <hr className='my-4 h-1 w-full border-0 bg-fade-hr' />

      <div className='flex justify-center'>
        <span className='text-xl font-bold text-[#FFE132] md:text-4xl'>
          {Sl2421.free_games_feature_blue_piggy}
        </span>
      </div>
      <ul className='flex list-disc list-image-[url(/images/e/icon_star.png)] flex-col space-y-6 pl-5 text-xl font-normal md:text-2xl'>
        {Sl2421.free_games_feature_blue_piggy_description.map(
          (game: string, index: number) => (
            <li key={index}>
              <DynamicContent text={game} />
            </li>
          )
        )}
      </ul>

      <hr className='my-4 h-1 w-full border-0 bg-fade-hr' />

      <div className='flex justify-center'>
        <span className='text-xl font-bold text-[#FFE132] md:text-4xl'>
          {Sl2421.free_games_feature_yellow_piggy}
        </span>
      </div>
      <ul className='flex list-disc list-image-[url(/images/e/icon_star.png)] flex-col space-y-6 pl-5 text-xl font-normal md:text-2xl'>
        {Sl2421.free_games_feature_yellow_piggy_description.map(
          (game: string, index: number) => (
            <li key={index}>
              <DynamicContent text={game} />
            </li>
          )
        )}
      </ul>

      <hr className='my-4 h-1 w-full border-0 bg-fade-hr' />

      <div className='flex justify-center'>
        <span className='text-xl font-bold text-[#FFE132] md:text-4xl'>
          {Sl2421.free_games_feature_red_piggy}
        </span>
      </div>
      <ul className='flex list-disc list-image-[url(/images/e/icon_star.png)] flex-col space-y-6 pl-5 text-xl font-normal md:text-2xl'>
        {Sl2421.free_games_feature_red_piggy_description.map(
          (game: string, index: number) => (
            <li key={index}>
              <DynamicContent text={game} />
            </li>
          )
        )}
      </ul>

      <hr className='my-4 h-1 w-full border-0 bg-fade-hr' />

      <div className='flex justify-center'>
        <span className='text-xl font-bold text-[#FFE132] md:text-4xl'>
          {Sl2421.pay_lines}
        </span>
      </div>

      <div className='flex flex-col space-y-2'>
        <img src='/images/e/Frame 10.png' alt='Frame 10.png' />
        <img src='/images/e/Frame 11.png' alt='Frame 11.png' />
        <img src='/images/e/Frame 12.png' alt='Frame 12.png' />
        <img src='/images/e/Frame 13.png' alt='Frame 13.png' />
        <img src='/images/e/Frame 14.png' alt='Frame 14.png' />
      </div>

      <hr className='my-4 h-1 w-full border-0 bg-fade-hr' />

      <div className='flex justify-center'>
        <span className='text-xl font-bold text-[#FFE132] md:text-4xl'>
          {Sl2421.disclaimer}
        </span>
      </div>
      <ul className='flex list-disc list-image-[url(/images/e/icon_star.png)] flex-col space-y-6 pl-5 text-xl font-normal md:text-2xl'>
        {Sl2421.disclaimer_description.map((game: string, index: number) => (
          <li key={index}>
            <DynamicContent text={game} />
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Sl2421;
