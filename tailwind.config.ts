import type { Config } from 'tailwindcss';

const config: Config = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      backgroundImage: {
        'fade-hr':
          'linear-gradient(to right, transparent 0%,  #777773 50%,  transparent 100%)',
      },
    },
  },
  plugins: [],
};
export default config;
